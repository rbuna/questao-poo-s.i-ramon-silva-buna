package Biblioteca;

public class Item {
	private String title;
	private String publisher;
	private int yearPublished;
	private double price;
	
	public Item (String title, String publisher, int yearPublished, String isbn, double price) {
		super ();
		this.title = title;
		this.yearPublished = yearPublished;
		this.isbn = isbn;
		this.publisher = publisher;
		this.price = price;
		}
	public void display() {
		System.out.println("Titulo"+this.title+"\nEditora:"+this.publisher+"\nData:"+this.yearPublished+"\nISBN"+ this.isbn+"\nPreco:"+ this.price);
		}
	public String getTitle() {
		return title;
		}
	public void  setTitle(String title) {
		this.title = title;
		}
	public String getPublisher() {
		return publisher;
		}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
		}
	public int getYearPublished() {
		return yearPublished;
		}
	public void setYearPublished(int yearPublished) {
		this.yearPublished = yearPublished;
		}
	
	public String getIsbn() {
		return isbn;
		}
	public void setIsbn(String Isbn) {
		this.isbn = isbn;
		}
	public double getPrice(double price) {
		this.price = price;
		}